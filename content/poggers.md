# Hi I'm Mojo!

Nice to meet you!

This is my crazy shell based website.

I'm a programmer that also loves to play with computers.

I waste a lot of time playing steam games too.

![Mojo with the cheese](WhereIsTheCheeseMojo.png) ![Mojo in a mask](MojoStaySafe.png) ![Happy mojo](MojoHappy.png)

<sub> Art by [@Weaveasy](https://twitter.com/Weaveasy) </sub>

Have a bunch of my links in this folder:
